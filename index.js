const { BlobServiceClient } = require("@azure/storage-blob");

// Azure blob storage connection config
const blobSasUrl = "https://nestinggamestorage.blob.core.windows.net/?sv=2021-06-08&ss=bf&srt=o&sp=ctfx&se=2023-07-07T21:21:50Z&st=2022-07-07T13:21:50Z&spr=https&sig=q%2BVCp1V5%2Fjk4dQ3FcvOBRE8zKftwP9NLgr4tMZKnvJM%3D";
const containerName = "user-session-log-files";

// Create a new BlobServiceClient
const blobServiceClient = new BlobServiceClient(blobSasUrl);

// Get a container client from the BlobServiceClient
const containerClient = blobServiceClient.getContainerClient(containerName);

// Demo UI
const selectButton = document.getElementById("select-button");
const fileInput = document.getElementById("file-input");
const status = document.getElementById("status");

const reportStatus = message => {
    status.innerHTML += `${message}<br/>`;
    status.scrollTop = status.scrollHeight;
}


const uploadFiles = async () => {
    try {
        reportStatus("Uploading files...");
        const promises = [];
        for (const file of fileInput.files) {
            const blockBlobClient = containerClient.getBlockBlobClient(file.name);
            promises.push(blockBlobClient.uploadBrowserData(file));
        }
        await Promise.all(promises);
        reportStatus("Done.");
    }
    catch (error) {
        reportStatus(error.message);
    }
}

selectButton.addEventListener("click", () => fileInput.click());
fileInput.addEventListener("change", uploadFiles);